from memory_profiler import memory_usage
import os
import time
import boto3
import psycopg2
import json
import urllib
import boto3
from multiprocessing import Process, Pipe
from time import sleep
import sys



def setTimeout(uid,time):
    sleep(time)
    print "AWS Lambda Timeout"
    updateDB(uid,"Time Limit Exceeded")

def compareOutput(file1, file2):
    f1 = open(file1,"r")
    f2 = open(file2,"r")
    if f1.read().strip() == f2.read().strip():
        f1.close()
        f2.close()
        print "Files match"
        return True
    else:
        f1.close()
        f2.close()
        print "Files dont match"
        return False

def updateDB(uid,status,score=None):
    conn = psycopg2.connect("dbname='cantilever' user='master_user' host='cant-db-test.ctcgws5j9pky.ap-south-1.rds.amazonaws.com' password='master_user_password123'")
    cur = conn.cursor()
    queru = """UPDATE test_site_codingquestionresponse
    SET status = %s WHERE unique_id = %s;
    """
    cur.execute(queru,(status,uid))
    if score != None:
        queru = """UPDATE test_site_codingquestionresponse
        SET score = %s WHERE unique_id = %s;
        """
        cur.execute(queru,(str(score),uid))
    conn.commit()
    conn.close()
    print "Updated Status"


def getLimits(qname):
    conn = psycopg2.connect("dbname='cantilever' user='master_user' host='cant-db-test.ctcgws5j9pky.ap-south-1.rds.amazonaws.com' password='master_user_password123'")
    cur = conn.cursor()
    query = """SELECT * FROM test_site_codingquestions
    WHERE question_name='%s';
    """ %(qname)
    cur.execute(query)
    rows = cur.fetchall()
    rows = rows[0]
    print rows
    conn.close()
    marks = rows[-5]
    memory_limit = rows[-4]
    time_limit = rows[-3]
    num_cases = rows[-2]
    weights = rows[-1]
    weights = [int(i) for i in weights.split(",")]
    print "Fetched Details"
    return memory_limit, time_limit, num_cases, weights,marks


def runCode(cmd):
    print "Executing Code"
    os.system(cmd)
    print "Executed Code"
    return

def getCommand(filename, ext, qname, testcase, uid):
    input_file = "%s_in_%s" %(qname,str(testcase))
    user_out = "%s_user_%s_%s" %(qname,str(testcase),uid)
    output_file = "%s_out_%s" %(qname,str(testcase))
    if ext == "py":
        command = "python %s < %s > %s" %(filename, input_file, user_out)
    elif ext == "class":
        java_name = filename.split(".")[0]
        command = "java %s < %s > %s" %(java_name, input_file, user_out)
    elif ext == "js":
        command = "node %s < %s > %s" %(filename, input_file, user_out)
    elif ext == "co" or "cppo":
        ch_perm = "chmod 775 %s" %(filename)
        os.system(ch_perm)
        command = "./%s < %s > %s" %(filename, input_file, user_out)
    return command, input_file, output_file, user_out

def downloadTestCases(qname,num_tests):
    s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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",region_name="ap-south-1")
    print "here"
    for i in range(1, num_tests+1):
        file_name = "%s_out_%s" %(qname, str(i))
        s3.Bucket('cantilever-test-cases').download_file(file_name, file_name)
        file_name = "%s_in_%s" %(qname, str(i))
        s3.Bucket('cantilever-test-cases').download_file(file_name, file_name)
        print "Downloaded Test Cases"
    return



def evaluateSubmission(key_name):
    filename = key_name
    filename = filename.split(".")
    lang_ext = filename[-1]
    filename = filename[0].split("_")
    uid = filename[0]
    qname = filename[-1]
    updateDB(uid,"Evaluating")
    memory_limit, time_limit, num_cases, weights, marks = getLimits(qname)
    downloadTestCases(qname,num_cases)
    score = 0
    weight = sum(weights)

    status = ""

    for i in range(1, num_cases +1):
        print "Evaluating test: ", i
        cmd, input_file, output_file, user_out = getCommand(key_name,lang_ext,qname,i,uid)
        print "Command: ",cmd
        start_time = time.time()
        mem = memory_usage((runCode, (cmd,), {}), timeout=None)
        exec_time = time.time() - start_time
        # print mem
        mem = max(mem)
        # print exec_time
        print "Memory: %d, Time: %d" %(mem, exec_time)
        # print
        if mem > memory_limit:
            status = "Memory Limit Exceeded"
            break
        elif exec_time > time_limit:
            status = "Time Limit Exceeded"
            break
        else:
            ccheck = compareOutput(user_out, output_file)
            if ccheck:
                score += (weights[i-1]*marks)/weight

    if score == marks:
        status = "Correct"
    elif score > 0:
        status = "Partially Correct"

    if status == "":
        status = "Wrong Answer"

    return uid, status,score





s3_client = boto3.client('s3')
def handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key'] 
        os.chdir("/tmp/")
        s3_client.download_file(bucket, key, key)
        # key = "aff655cb5d3a141c1af4f8abefdeebe8a_helloworld.py"
        uid = key.split("_")[0]
        queue = Pipe()
        p = Process(target=setTimeout, args=(uid,60))
        p.start()
        uid, status,score = evaluateSubmission(key)
        updateDB(uid, status,score)
        p.terminate()
        

# os.chdir("./tmp/")
#         # s3_client.download_file(bucket, key, key)
# key = "a511bcbe4d296409a8acec77f52e38de0_helloworld.js"
# uid = key.split("_")[0]
# # queue = Pipe()
# # p = Process(target=setTimeout, args=(uid,60))
# # p.start()
# uid, status,score = evaluateSubmission(key)
# updateDB(uid, status,score)
# # p.terminate()
