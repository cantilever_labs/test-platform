"""portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from test_site import views
from django.conf.urls import include


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', views.login_view, name='login'),
    url(r'^register/', views.register_view, name='register'),
    url(r'^mcq/', views.mcq_view, name='mcq_view'),
    url(r'^text/', views.text_view, name='text_view'),
    url(r'^oj/', views.oj_view, name='oj_view'),
    url(r'^submissions/', views.oj_subs, name='oj_subs'),
    url(r'^complete/', views.test_complete, name='test_complete'),
    url(r'^feedback/', views.feedback, name='feedback'),
    url(r'^thankyou/', views.thankyou, name='thankyou'),
    url(r'^begin-test/', views.begin_test, name='begin_test'),
    url(r'^test_instructions/', views.test_instructions, name='instructions'),
    url(r'^section_instructions/', views.section_instructions, name='instructions'),
    url(r'^2bcd6fb3d1314ff68b775fc4ca82164d/', views.save_session, name='save_session'),
    url(r'^2bcd6fb3d1asd314ff68b775fc4ca82164d/', views.save_session_oj, name='save_session_oj'),
    url(r'^2bcd6fb3d1asd314ffsadfg75fc4ca82164d/', views.save_session_text, name='save_session_text'),
    url(r'^assessment/', views.create_session, name='create_session'),
    url(r'^upload/', views.upload, name='upload'),
    url(r'^create/', views.create, name='create'),
    url(r'^delete/', views.delete, name='delete'),
    
    url(r'^memory-game/', views.memory_game, name='memory_game'),
    url(r'^number-chronology/', views.number_chronology, name='number_chronology'),
    url(r'^7-up/', views.seven_up, name='seven_up'),
    url(r'^moneyball/', views.moneyball, name='moneyball'),

    # url(r'^sample/memory-game/', views.memory_game_sample, name='memory_game_sample'),
    # url(r'^sample/number-chronology/', views.number_chronology_sample, name='number_chronology_sample'),
    # url(r'^sample/7-up/', views.seven_up_sample, name='seven_up_sample'),
    # url(r'^sample/moneyball/', views.moneyball_sample, name='moneyball_sample'),
    
    url(r'^test_login/token/(?P<access_token>\S+)/$',views.login_access, name = 'login_key'),
    url(r'^.*', views.login_view, name='login'),
    
]
