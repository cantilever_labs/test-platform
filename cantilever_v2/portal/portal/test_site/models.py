# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
import random,string

class GameConfiguration(models.Model):
    game_id = models.CharField(max_length=200,unique=True)
    grid_rows = models.PositiveIntegerField()
    grid_cols = models.PositiveIntegerField()
    flash_time = models.PositiveIntegerField()
    num_lives = models.PositiveIntegerField()
    timeout = models.PositiveIntegerField()
    level_transition_time = models.PositiveIntegerField()
    input_persistance_time = models.IntegerField()
    persist_wrong_input = models.PositiveIntegerField()
    persist_until_input = models.BooleanField()
    

    def __unicode__(self):
       return self.game_id



class GameResult(models.Model):
    user = models.ForeignKey(User)
    score = models.IntegerField(null=True)
    lives = models.IntegerField(null=True)
    time_stamp = models.DateTimeField()
    game  = models.CharField(max_length=100)

class MoneyballLog(models.Model):
    game_session = models.ForeignKey(GameResult)
    level = models.IntegerField()
    event = models.CharField(max_length=100,null=True)
    time_elapsed = models.IntegerField()   


class MoneyballAction(models.Model):
    game_log = models.ForeignKey(MoneyballLog)
    amount = models.IntegerField()
    market = models.CharField(max_length=100,null=True)
    action = models.CharField(max_length=100,null=True)



class SevenUpLog(models.Model):
    game_session = models.ForeignKey(GameResult)
    level = models.IntegerField()
    depsoited_7up = models.IntegerField()
    depsoited_7 = models.IntegerField()
    depsoited_7down = models.IntegerField()
    dice1_value = models.IntegerField()
    dice2_value = models.IntegerField()
    profit_7up = models.IntegerField()
    profit_7 = models.IntegerField()
    profit_7down =models.IntegerField()
    event = models.CharField(max_length=100,null=True)
    time_elapsed = models.IntegerField()   


class GameLog(models.Model):
    game_session = models.ForeignKey(GameResult)
    level = models.IntegerField()
    grid_x = models.IntegerField()
    grid_y = models.IntegerField()
    score = models.IntegerField()
    lives = models.IntegerField()
    event = models.CharField(max_length=100,null=True)
    input_name = models.CharField(max_length=100)
    expected = models.BooleanField()
    time_elapsed = models.IntegerField()   

class SessionKey(models.Model):
    session_key = models.CharField(max_length=200,unique=True, default=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(12)))
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    def __unicode__(self):
       return self.session_key

class ChoiceList(models.Model):
    choice_type = models.CharField(max_length=200)
    choice_value = models.CharField(max_length=200,unique=True)


class SiteConfiguration(models.Model):
    config_key = models.CharField(max_length=200,unique=True)
    config_value = models.CharField(max_length=200)
    
    def __unicode__(self):
       return self.config_key



class UserProfile(models.Model):
    user = models.OneToOneField(User)
    college_name = models.CharField(max_length=200)
    branch = models.CharField(max_length=200)
    degree = models.CharField(max_length=200)
    student_id = models.CharField(max_length=200)
    cgpa = models.DecimalField(max_digits=4, decimal_places=2) 
    mobile = models.CharField(max_length=10)
    expected_year = models.DateField()
    dob = models.DateField()
    user_email = models.CharField(max_length=200)

    home_town = models.CharField(max_length=200)
    home_state = models.CharField(max_length=200)
    work_experience = models.CharField(max_length=200)
    years_experience = models.PositiveIntegerField()
    org_name  = models.CharField(max_length=200)


    user_email = models.CharField(max_length=200)

    marks_12 = models.DecimalField(max_digits=5, decimal_places=2) 
    marks_10 = models.DecimalField(max_digits=5, decimal_places=2)

    def __unicode__(self):
       return self.user.first_name

class UserFeedback(models.Model):
    user = models.ForeignKey(User)
    q4 = models.CharField(max_length=200)
    q5 = models.CharField(max_length=200)
    q1 = models.DecimalField(max_digits=4, decimal_places=2) 
    q2 = models.DecimalField(max_digits=4, decimal_places=2) 
    q3 = models.DecimalField(max_digits=4, decimal_places=2) 

    def __unicode__(self):
       return self.user.first_name

class AssessmentType(models.Model):
    assessment_type = models.CharField(max_length=200,unique=True)    

class Assessment(models.Model):
    test_name = models.CharField(max_length=200,unique=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    assessment_type = models.ForeignKey(AssessmentType,null=True)


    def __unicode__(self):
       return self.test_name


class UserAccessKeys(models.Model):
    user = models.ForeignKey(User)
    assessment = models.ForeignKey(Assessment)
    access_token = models.CharField(max_length=200,unique=True)

    def __unicode__(self):
       return self.user.username + "-" + self.assessment.test_name




class AssessmentSection(models.Model):
    section_name = models.CharField(max_length=2000)
    assessment = models.ForeignKey(Assessment)
    section_duration = models.PositiveIntegerField()
    section_type = models.CharField(max_length=2000, default="mcq")

    def __unicode__(self):
       return self.section_name

class MCQQuestions(models.Model):
    question_body = models.CharField(max_length=200000)
    img_url = models.URLField(blank=True,null=True)
    section = models.ForeignKey(AssessmentSection,null=True)
    marks = models.PositiveIntegerField()
    negative_marks = models.FloatField(default=0)
    passage = models.BooleanField()
    passage_obj= models.ForeignKey('MCQPassage',null=True)
    tag1 = models.CharField(max_length=2000,null=True,blank=True) # Topic
    tag2 = models.CharField(max_length=2000,null=True,blank=True) # Difficulty
    tag3 = models.CharField(max_length=2000,null=True,blank=True)
    tag4 = models.CharField(max_length=2000,null=True,blank=True)
    tag5 = models.CharField(max_length=2000,null=True,blank=True)

    def __unicode__(self):
       return self.question_body

class MCQPassage(models.Model):
    passage_body = models.CharField(max_length=3000,null = True,blank=True)
    passage_identifier = models.CharField(max_length=300)

    def __unicode__(self):
       return self.passage_identifier



class MCQChoices(models.Model):
    choice_body = models.CharField(max_length=200)
    question = models.ForeignKey(MCQQuestions,null=True)
    correct_flag = models.BooleanField()

    def __unicode__(self):
       return self.choice_body


class CodingQuestions(models.Model):
    question_body = models.CharField(max_length=20000)
    question_name = models.CharField(max_length=200,unique=True)
    section = models.ManyToManyField(AssessmentSection)
    marks = models.PositiveIntegerField()
    memory_limit = models.PositiveIntegerField(default=0)
    time_limit = models.PositiveIntegerField(default=0)
    num_cases = models.PositiveIntegerField(default=1)
    weights = models.CharField(max_length=100)

    def __unicode__(self):
       return self.question_name

class UserDefaultSession(models.Model):
    user = models.ForeignKey(User)
    default_state = models.CharField(max_length=20000)
    assessment = models.ForeignKey(Assessment,null=True)

    def __unicode__(self):
       return self.user.username

class UserActive(models.Model):
    user = models.OneToOneField(User)
    logged_in = models.BooleanField(default=False)


class UserSessionState(models.Model):
    user = models.ForeignKey(User)
    current_section = models.CharField(max_length=20000)
    current_question = models.CharField(max_length=20000)
    time_left = models.CharField(max_length=200)
    section_overview = models.CharField(max_length=20000)
    ended =models.BooleanField(default=False)
    assessment = models.ForeignKey(Assessment,null=True)

    def __unicode__(self):
       return self.user.username

class AssessmentInstruction(models.Model):
    assessment = models.ManyToManyField(Assessment)
    instruction = models.CharField(max_length=2000)
    sequence = models.PositiveIntegerField(default=1)

    def __unicode__(self):
       return self.instruction

class SectionInstruction(models.Model):
    section = models.ManyToManyField(AssessmentSection)
    instruction = models.CharField(max_length=2000)
    sequence = models.PositiveIntegerField(default=1)

    def __unicode__(self):
       return self.instruction
    


class MCQResponse(models.Model):
    user = models.ForeignKey(User)
    section = models.ForeignKey(AssessmentSection)
    question = models.ForeignKey(MCQQuestions)
    user_response = models.ForeignKey(MCQChoices,null=True)

    def __unicode__(self):
       return self.user.username
    
class CodingQuestionResponse(models.Model):
    user = models.ForeignKey(User)
    code_body = models.CharField(max_length=20000,null=True)
    code_language = models.CharField(max_length=200)
    question_name = models.CharField(max_length=200)
    section = models.ForeignKey(AssessmentSection,null=True)
    score = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=100,null=True)
    unique_id = models.CharField(max_length=100,null=True)
    submitted = models.BooleanField(default=False)

    def __unicode__(self):
       return self.user.username

class TextQuestion(models.Model):
    question_body = models.CharField(max_length=2000)
    img_url = models.URLField(blank=True,null=True)
    section = models.ForeignKey(AssessmentSection)
    correct_answer = models.CharField(max_length=2000)
    marks = models.PositiveIntegerField()
    negative_marks = models.FloatField(default=0)
    passage = models.BooleanField()
    passage_obj= models.ForeignKey('MCQPassage',null=True)
    tag1 = models.CharField(max_length=2000,null=True,blank=True) # Topic
    tag2 = models.CharField(max_length=2000,null=True,blank=True) # Difficulty
    tag3 = models.CharField(max_length=2000,null=True,blank=True)
    tag4 = models.CharField(max_length=2000,null=True,blank=True)
    tag5 = models.CharField(max_length=2000,null=True,blank=True)

    def __unicode__(self):
       return self.question_body

class TextQuestionResponse(models.Model):
    user = models.ForeignKey(User)
    user_response = models.CharField(max_length=2000,null=True)
    section = models.ForeignKey(AssessmentSection,null=True)
    status = models.CharField(max_length=100,null=True)
    question = models.ForeignKey(TextQuestion)
    

    def __unicode__(self):
       return self.user.username + " - " + self.question.question_body


class GameConfiguration(models.Model):
    game_id = models.CharField(max_length=200,unique=True)
    grid_rows = models.PositiveIntegerField()
    grid_cols = models.PositiveIntegerField()
    flash_time = models.PositiveIntegerField()
    num_lives = models.PositiveIntegerField()
    timeout = models.PositiveIntegerField()
    level_transition_time = models.PositiveIntegerField()
    input_persistance_time = models.IntegerField()
    persist_wrong_input = models.PositiveIntegerField()
    persist_until_input = models.BooleanField()
    

    def __unicode__(self):
       return self.game_id

# class GameResult(models.Model):
#     game_id = models.CharField(max_length=200,null=True,unique=True)
#     grid_rows = models.PositiveIntegerField()
#     gris_cols = models.PositiveIntegerField()
#     flash_time = models.PositiveIntegerField()
#     num_lives = models.PositiveIntegerField()
#     timeout = models.PositiveIntegerField()
#     level_tranistion_time = models.PositiveIntegerField()
#     input_persitance_time = models.IntegerField()
#     persist_until_input = models.BooleanField()
    

#     def __unicode__(self):
#        return self.game_id

# class GameLog(models.Model):
#     game_id = models.CharField(max_length=200,null=True,unique=True)
#     grid_rows = models.PositiveIntegerField()
#     gris_cols = models.PositiveIntegerField()
#     flash_time = models.PositiveIntegerField()
#     num_lives = models.PositiveIntegerField()
#     timeout = models.PositiveIntegerField()
#     level_tranistion_time = models.PositiveIntegerField()
#     input_persitance_time = models.IntegerField()
#     persist_until_input = models.BooleanField()
    

#     def __unicode__(self):
#        return self.game_id
