# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import *

admin.site.register(UserProfile)
admin.site.register(UserFeedback)
admin.site.register(Assessment)
admin.site.register(AssessmentSection)
admin.site.register(MCQQuestions)
admin.site.register(MCQChoices)
admin.site.register(CodingQuestions)
admin.site.register(MCQPassage)
admin.site.register(MCQResponse)
admin.site.register(UserDefaultSession)
admin.site.register(UserSessionState)
admin.site.register(CodingQuestionResponse)
admin.site.register(AssessmentInstruction)
admin.site.register(SectionInstruction)
admin.site.register(SessionKey)
admin.site.register(UserAccessKeys)
admin.site.register(SiteConfiguration)
admin.site.register(TextQuestion)
admin.site.register(TextQuestionResponse)
admin.site.register(GameConfiguration)
admin.site.register(ChoiceList)
admin.site.register(GameResult)
admin.site.register(AssessmentType)
admin.site.register(SevenUpLog)


# class MCQPassageForm(forms.ModelForm):

#     class Meta:
#         model = MCQPassage
        

# class PersonAdmin(admin.ModelAdmin):
#     # exclude = ['age']
#     form = MCQPassageForm