# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-11 01:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('test_site', '0029_auto_20190116_1111'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('assessment_type', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.AlterField(
            model_name='sessionkey',
            name='session_key',
            field=models.CharField(default='UKC9PF3Y5UMY', max_length=200, unique=True),
        ),
        migrations.AddField(
            model_name='assessment',
            name='assessment_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.AssessmentType'),
        ),
    ]
