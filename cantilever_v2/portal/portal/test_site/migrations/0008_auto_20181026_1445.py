# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-26 09:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('test_site', '0007_auto_20181026_1409'),
    ]

    operations = [
        migrations.CreateModel(
            name='TextQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_body', models.CharField(max_length=2000)),
                ('img_url', models.URLField(blank=True, null=True)),
                ('correct_answer', models.CharField(max_length=2000)),
                ('marks', models.PositiveIntegerField()),
                ('negative_marks', models.FloatField(default=0)),
                ('passage', models.BooleanField()),
                ('tag1', models.CharField(blank=True, max_length=2000, null=True)),
                ('tag2', models.CharField(blank=True, max_length=2000, null=True)),
                ('tag3', models.CharField(blank=True, max_length=2000, null=True)),
                ('tag4', models.CharField(blank=True, max_length=2000, null=True)),
                ('tag5', models.CharField(blank=True, max_length=2000, null=True)),
                ('passage_obj', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.MCQPassage')),
                ('section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='test_site.AssessmentSection')),
            ],
        ),
        migrations.AlterField(
            model_name='sessionkey',
            name='session_key',
            field=models.CharField(default='M135TY6N7158', max_length=200, unique=True),
        ),
    ]
