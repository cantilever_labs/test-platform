# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-21 02:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_site', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionkey',
            name='session_key',
            field=models.CharField(default='OXJ8NC6GFYNG', max_length=200, unique=True),
        ),
    ]
