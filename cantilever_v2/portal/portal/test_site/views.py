# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess, threading, os
import psycopg2
from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import LoginForm, UserDetailsForm, FeedbackForm,OJForm,MCQForm, TextForm
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from models import *
import uuid
import boto3
import os
import random,string
import json
from collections import OrderedDict
import datetime
from django.utils import timezone
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth.hashers import make_password



# UPLOADED_TEST = SiteConfiguration.objects.get(config_key="uploaded_test")

#### Helpers for OJ

def updateDB(uid,status):
	conn = psycopg2.connect("dbname='cantilever' user='master_user' host='cant-db-test.ctcgws5j9pky.ap-south-1.rds.amazonaws.com' password='master_user_password123'")
	cur = conn.cursor()
	queru = """UPDATE test_site_codingquestionresponse
	SET status = %s WHERE unique_id = %s;
	"""
	cur.execute(queru,(status,uid))
	conn.commit()
	conn.close()
	print "Updated Status"


def uploadCode(sub_id,question_name,code_language,code_body):
	code_ext = {
	"Python 2.7":"py",
	"C":"c",
	"C++":"cpp",
	"JavaScript":"js",
	"Java":"java",
	}
	if code_language == "Python 2.7" or code_language == "JavaScript":
		file_ext = code_ext[code_language]
		file_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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",region_name="ap-south-1")
		s3.Bucket('cantilever-user-codes').put_object(Key=file_name, Body=code_body)

	elif code_language == "C":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		compiled_file = "%s_%s.co" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("gcc -o %s %s" %(compiled_file,source_name))
		if ccheck == 0:
			# os.system("chmod u+x %s" %(compiled_file))
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")
			

	elif code_language == "C++":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		compiled_file = "%s_%s.cppo" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("g++ -o %s %s" %(compiled_file,source_name))
		print "CCheck: ",ccheck
		if ccheck == 0:
			print "Uploading"
			# os.system("chmod u+x %s" %(compiled_file))
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")

	elif code_language == "Java":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_code = source_code.replace("SolutionClass", sub_id+"_"+question_name)
		source_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		compiled_file = "%s_%s.class" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("javac %s" %(source_name))
		if ccheck == 0:
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")

### Config for OJ

##### Periodic save for mcq

@login_required(login_url='/login/',redirect_field_name=None)
def save_session(request):
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	# if type(current_session["time_left"]) == type("asd"):
	# 	current_session["time_left"] = json.loads(current_session["time_left"])
	
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = MCQQuestions.objects.get(pk=question_id)
	# # print section.section_name
	# print "Save request finction"
	if request.method == 'POST':
		# print "Save session request received"
		form = MCQForm(request.POST)
	
		if form.is_valid():
			if MCQResponse.objects.filter(user=request.user,section=section,question=question).exists():
				try:
					user_response = MCQResponse.objects.get(user=request.user,section=section,question=question)
				except MultipleObjectsReturned:
					user_response = MCQResponse.objects.filter(user=request.user,section=section,question=question).first()

				try:
					choice_selected = request.POST["options"]
					choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = choice
					selected_option = choice.id
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				
				user_response.save()
			else:
				user_response = MCQResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question = question
				# selected_option = choice_selected
				try:
					choice_selected = request.POST["options"]
					choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = choice
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				user_response.save()
			action_f = form.cleaned_data["bsource"]
			qno_next =form.cleaned_data["qno"]
			sno =form.cleaned_data["sno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])
			request.session, ended,section_change = updateCurrentSession(action_f,request,time_left,qno_next,ques_attempted,sno)
			message = "Saved"
			return HttpResponse(json.dumps({'message': message}))		


##### Periodic save for Text
@login_required(login_url='/login/',redirect_field_name=None)
def save_session_text(request):
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	# if type(current_session["time_left"]) == type("asd"):
	# 	current_session["time_left"] = json.loads(current_session["time_left"])
	
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = TextQuestion.objects.get(pk=question_id)
	# # print section.section_name
	# print "Save request finction"
	if request.method == 'POST':
		form = TextForm(request.POST)
		print request.POST
	
		if form.is_valid():
			if TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).exists():
				try:
					user_response = TextQuestionResponse.objects.get(user=request.user,section=section,question=question)
				except MultipleObjectsReturned:
					user_response = TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).first()
				try:
					user_answer = request.POST["user_response"]
					# choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = user_answer
					# selected_option = choice.id
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					# selected_option = None
				
				user_response.save()
			else:
				user_response = TextQuestionResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question = question
				
				try:
					user_answer = request.POST["user_response"]
					user_response.user_response = user_answer
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				user_response.save()
			action_f = form.cleaned_data["bsource"]
			qno_next =form.cleaned_data["qno"]
			sno =form.cleaned_data["sno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])

			request.session, ended,section_change = updateCurrentSessionText(action_f,request,time_left,qno_next,ques_attempted,sno)
			message = "Saved"
			return HttpResponse(json.dumps({'message': message}))		



#### Periodic Save for OJ

@login_required(login_url='/login/',redirect_field_name=None)
def save_session_oj(request):
	print "Save session req recvd"	
	#### Resume session
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	
	#### Check if session has ended
	if current_session["ended"]:
		return redirect("/complete/")
	

	#### Get current status from session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_question = current_session["current_question"]
	

	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	sec_id = sections_list[int(current_section)]
	
	if type(current_session["time_left"]) == type("asd"):
		current_session["time_left"] = json.loads(current_session["time_left"])
	

	section = AssessmentSection.objects.get(pk=sec_id)
	
	if section.section_type == "mcq":
		return redirect("/mcq/")

	section_name = section.section_name

	time_left = current_session["time_left"][str(sec_id)]
	# time_left = 900
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = CodingQuestions.objects.get(pk=question_id)
	question_name = question.question_name
	question_text = question.question_body

	# question_name = "Sample Question Name"
	# question_text = "Sample Question Body"

	timer_min = int(time_left)/60
	timer_sec = int(time_left)%60
		

	form = OJForm()
	ques_overview = section_overview["status"][sec_id]
	answered = 0
	unanswered = 0
	flagged = 0
	for q in ques_overview:
		if str(q["status"]) == "unanswered":
			unanswered += 1
		elif str(q["status"]) == "answered":
			answered += 1
		elif str(q["status"]) == "flagged":
			flagged += 1

	if request.method == 'POST':
		form = OJForm(request.POST)
		if form.is_valid():
			print request.POST
			action_f = form.cleaned_data["bsource"]
			if CodingQuestionResponse.objects.filter(user=request.user,section=section,question_name=question_name).exists():
				user_response = CodingQuestionResponse.objects.get(user=request.user,section=section,question_name=question_name)
				user_response.code_body = form.cleaned_data["code_body"]
				user_response.code_language = form.cleaned_data["code_language"]
				user_response.status = "Queued"
				
				if action_f == "bsub":
					user_response.submitted = True
				user_response.save()
				print "Updated DB"
			else:
				user_response = CodingQuestionResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question_name = question_name
				user_response.code_body = form.cleaned_data["code_body"]
				user_response.code_language = form.cleaned_data["code_language"]
				user_response.status = "Queued"
				
				if action_f == "bsub":
					user_response.submitted = True
				user_response.save()
				print "Saved to DB"

			
			if action_f == "bsub":
				ques_attempted = True
			qno_next =form.cleaned_data["qno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])

			request.session, ended,section_change = updateCurrentSessionOJ(action_f,request,time_left,qno_next,ques_attempted)

			if section_change == 1:
				return redirect("/section_instructions/")
				
			if ended:
				request.session["ended"] = True
				return redirect("/complete/")

			message = "Saved"
		else:
			print form.errors
		return HttpResponse(json.dumps({'message': message}))		



# End of config


def updateCurrentSessionGame(request):
	section_change = 0
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_section = current_session["current_section"]
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	ended = False
	if int(current_section) < len(sections_list) -1:
		current_section = 1 + int(current_section)
		current_session["current_section"] = current_section
		section_object = AssessmentSection.objects.get(pk = sections_list[current_section])
		# current_session["time_left"][str()] = section_object.section_duration
		current_question = 0
		current_session["current_question"] = current_question
		section_change = 1
	else:
		current_session["ended"] = True
	# current_session["time_left"] = json.loads(current_session["time_left"])
	request.session["current_session"] = current_session
	session_dict = request.session["current_session"]
	current_session_db = UserSessionState.objects.get(user=request.user)
	current_session_db.current_section = session_dict["current_section"]
	current_session_db.ended = session_dict["ended"]
	current_session_db.current_question = session_dict["current_question"]
	current_session_db.time_left = json.dumps(current_session["time_left"])
	# print current_session_db.time_left
	current_session_db.section_overview = 	json.dumps(session_dict["section_overview"])
	current_session_db.save()

	return request.session,ended,section_change


def updateCurrentSessionText(action_f,request,timer_var,qno_next=None,ques_attempted=False,sno=None):
	ended = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	# print current_session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	time_left = current_session["time_left"]
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = TextQuestion.objects.get(pk=question_id)
	if ques_attempted :
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "answered"
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
	else:
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "unanswered"
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
	# current_session["time_left"] = json.loads(current_session["time_left"])
	# current_session["time_left"][str(sec_id)] = timer_var	
	
	if action_f == "bnext":
		if (int(current_question) + 1)< len(default_session["sections"][sec_id]):
			current_question = int(current_question) + 1
			current_session["current_question"] = current_question
	elif action_f == "bprev":
		if (int(current_question)) > 0:
			current_question = int(current_question) - 1
			current_session["current_question"] = current_question
	elif action_f == "brev":
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if ques_attempted==False and q["status"] == "flagged":
					q["status"] = "unanswered"
				elif ques_attempted==True and q["status"] == "flagged":
					q["status"] = "answered"
				else:
					q["status"] = "flagged"
					
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
		
	elif action_f == "bnav":
		current_question = int(qno_next) - 1
		current_session["current_question"] = current_question

	section_change = 0	
	if int(sec_id) == int(sno):
		print "Sections Match - Time Updated"
		current_session["time_left"][str(sections_list[int(current_section)])] = timer_var	


	if action_f == "nupd":
		# print "Session Saved"
		pass
	
	
	

	elif action_f == "bend":
		# print "reached here - bend"
		if int(sec_id) == int(sno):
			print "Sections match"
			ended = False
			if int(current_section) < len(sections_list) -1:
				current_section = 1 + int(current_section)
				current_session["current_section"] = current_section
				section_object = AssessmentSection.objects.get(pk = sections_list[current_section])
				# current_session["time_left"][str()] = section_object.section_duration
				current_question = 0
				current_session["current_question"] = current_question
				section_change = 1
			else:
				current_session["ended"] = True
	# current_session["time_left"] = json.loads(current_session["time_left"])
	request.session["current_session"] = current_session
	session_dict = request.session["current_session"]
	current_session_db = UserSessionState.objects.get(user=request.user)
	current_session_db.current_section = session_dict["current_section"]
	current_session_db.ended = session_dict["ended"]
	current_session_db.current_question = session_dict["current_question"]
	current_session_db.time_left = json.dumps(current_session["time_left"])
	# print current_session_db.time_left
	current_session_db.section_overview = 	json.dumps(session_dict["section_overview"])
	current_session_db.save()

	# if action_f == "bend":
	# 	return redirect("/section_instructions/")

	return request.session,ended,section_change


def updateCurrentSession(action_f,request,timer_var,qno_next=None,ques_attempted=False,sno=None):
	ended = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	# print current_session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	time_left = current_session["time_left"]
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = MCQQuestions.objects.get(pk=question_id)
	if ques_attempted :
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "answered"
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
	else:
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "unanswered"
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
	# current_session["time_left"] = json.loads(current_session["time_left"])
	# current_session["time_left"][str(sec_id)] = timer_var	
	
	if action_f == "bnext":
		if (int(current_question) + 1)< len(default_session["sections"][sec_id]):
			current_question = int(current_question) + 1
			current_session["current_question"] = current_question
	elif action_f == "bprev":
		if (int(current_question)) > 0:
			current_question = int(current_question) - 1
			current_session["current_question"] = current_question
	elif action_f == "brev":
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if ques_attempted==False and q["status"] == "flagged":
					q["status"] = "unanswered"
				elif ques_attempted==True and q["status"] == "flagged":
					q["status"] = "answered"
				else:
					q["status"] = "flagged"
					
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
		
	elif action_f == "bnav":
		current_question = int(qno_next) - 1
		current_session["current_question"] = current_question

	section_change = 0	
	if int(sec_id) == int(sno):
		print "Sections Match - Time Updated"
		current_session["time_left"][str(sections_list[int(current_section)])] = timer_var	


	if action_f == "nupd":
		# print "Session Saved"
		pass
	
	
	

	elif action_f == "bend":
		# print "reached here - bend"
		if int(sec_id) == int(sno):
			print "Sections match"
			ended = False
			if int(current_section) < len(sections_list) -1:
				current_section = 1 + int(current_section)
				current_session["current_section"] = current_section
				section_object = AssessmentSection.objects.get(pk = sections_list[current_section])
				# current_session["time_left"][str()] = section_object.section_duration
				current_question = 0
				current_session["current_question"] = current_question
				section_change = 1
			else:
				current_session["ended"] = True
	# current_session["time_left"] = json.loads(current_session["time_left"])
	request.session["current_session"] = current_session
	session_dict = request.session["current_session"]
	current_session_db = UserSessionState.objects.get(user=request.user)
	current_session_db.current_section = session_dict["current_section"]
	current_session_db.ended = session_dict["ended"]
	current_session_db.current_question = session_dict["current_question"]
	current_session_db.time_left = json.dumps(current_session["time_left"])
	# print current_session_db.time_left
	current_session_db.section_overview = 	json.dumps(session_dict["section_overview"])
	current_session_db.save()

	# if action_f == "bend":
	# 	return redirect("/section_instructions/")

	return request.session,ended,section_change
			
### Update function for OJ

def updateCurrentSessionOJ(action_f,request,timer_var,qno_next=None,ques_attempted=False):
	ended = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_session = request.session["current_session"]
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	time_left = current_session["time_left"]
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = CodingQuestions.objects.get(pk=question_id)
	user_subs = CodingQuestionResponse.objects.get(user=request.user,question_name=question.question_name)
	if user_subs.submitted:
		green = True
	else:
		green = False

	
	
	if green:
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "answered"
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
	
	else:
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if q["status"] != "flagged":
					q["status"] = "unanswered"
				break
	
		section_overview["status"][sec_id] = ques_overview
	
		current_session["section_overview"] =section_overview
	# current_session["time_left"] = json.loads(current_session["time_left"])
	# current_session["time_left"][str(sec_id)] = timer_var	
	
	if action_f == "bnext":
		if (int(current_question) + 1)< len(default_session["sections"][sec_id]):
			current_question = int(current_question) + 1
			current_session["current_question"] = current_question
	elif action_f == "bprev":
		if (int(current_question)) > 0:
			current_question = int(current_question) - 1
			current_session["current_question"] = current_question
	elif action_f == "brev":
		ques_overview = section_overview["status"][sec_id]
		for q in ques_overview:
		
			if int(q["qno"]) == int(current_question)+1:
				if ques_attempted==False and q["status"] == "flagged":
					q["status"] = "unanswered"
				elif ques_attempted==True and q["status"] == "flagged":
					q["status"] = "answered"
				else:
					q["status"] = "flagged"
					
				break
		section_overview["status"][sec_id] = ques_overview
		current_session["section_overview"] =section_overview
		
	elif action_f == "bnav":
		current_question = int(qno_next) - 1
		current_session["current_question"] = current_question

	section_change = 0	

	current_session["time_left"][str(sections_list[int(current_section)])] = timer_var	


	if action_f == "nupd":
		# print "Session Saved"
		pass
	
	
	

	elif action_f == "bend":
		# print "reached here - bend"
		ended = False
		if int(current_section) < len(sections_list) -1:
			current_section = 1 + int(current_section)
			current_session["current_section"] = current_section
			section_object = AssessmentSection.objects.get(pk = sections_list[current_section])
			# current_session["time_left"][str()] = section_object.section_duration
			current_question = 0
			current_session["current_question"] = current_question
			section_change = 1
		else:
			current_session["ended"] = True
	# current_session["time_left"] = json.loads(current_session["time_left"])
	request.session["current_session"] = current_session
	session_dict = request.session["current_session"]
	current_session_db = UserSessionState.objects.get(user=request.user)
	current_session_db.current_section = session_dict["current_section"]
	current_session_db.ended = session_dict["ended"]
	current_session_db.current_question = session_dict["current_question"]
	current_session_db.time_left = json.dumps(current_session["time_left"])
	# print current_session_db.time_left
	current_session_db.section_overview = 	json.dumps(session_dict["section_overview"])
	current_session_db.save()

	# if action_f == "bend":
	# 	return redirect("/section_instructions/")

	return request.session,ended,section_change

### Login View

def login_view(request):
	errors = ""
	if request.method == 'POST':
		# print request.POST
		form = LoginForm(request.POST)
		if form.is_valid():
			request.session.flush()
			username = request.POST['user_id']
			password = request.POST['password']
			session_key = request.POST['session_key']
			print session_key
			if SessionKey.objects.filter(session_key=session_key).exists():
				print "SK Found"
				sk_obj = SessionKey.objects.get(session_key=session_key)
				now = timezone.now()
				if sk_obj.start_time <= now and sk_obj.end_time >= now:
					print "Time matched"
					user = authenticate(request, username=username, password=password)
					if user is not None:
						login(request, user)
						print "Redirecting"
						return redirect("/register/")
				else:
					errors =  "Invalid Credentials."

			else:
				errors =  "Invalid Credentials."
		else:
			print form.errors
		
	else:
		form = LoginForm()

	context = {
	
		'form':form,
		'error':errors,
		'time_now':datetime.datetime.now()
		
	}
	return render(request, 'test_site/login.html',context)


def login_access(request,access_token):
	errors = ""
	# try:
	token_entry = UserAccessKeys.objects.get(access_token=access_token)
	user = token_entry.user
	login(request, user)
	return redirect("/register/")
	# except Exception as excp:
	# 	return HttpResponse("Inavlid Token")



### Register View


@login_required(login_url='/login/',redirect_field_name=None)
def register_view(request):
	errors = ""
	current_user = request.user
	if UserProfile.objects.filter(user=current_user).exists():
		return redirect("/begin-test/")
	if request.method == 'POST':
		

		# print request.POST
		form = UserDetailsForm(request.POST)
		if form.is_valid():
			new_profile = UserProfile()
			print form.cleaned_data
			current_user.first_name = form.cleaned_data['name']
			new_profile.user =current_user
			new_profile.college_name = form.cleaned_data['InputCollegeName']
			new_profile.branch = form.cleaned_data['InputBranch']
			new_profile.student_id = form.cleaned_data['student_id']
			new_profile.cgpa = form.cleaned_data['cgpa']
			new_profile.mobile =form.cleaned_data['mobile']
			new_profile.expected_year  =form.cleaned_data['expected_year']
			new_profile.dob =form.cleaned_data['dob']
			new_profile.marks_10 =form.cleaned_data['marks_10']
			new_profile.marks_12 =form.cleaned_data['marks_12']
			new_profile.user_email =form.cleaned_data['user_email']
			
			new_profile.home_town =form.cleaned_data['home_town']
			new_profile.home_state =form.cleaned_data['home_state']
			new_profile.org_name =form.cleaned_data['org_name']
			new_profile.work_experience =form.cleaned_data['work_experience']
			new_profile.years_experience =form.cleaned_data['years_of_experience']
			current_user.email =form.cleaned_data['user_email']
			new_profile.save()
			current_user.save()
			print "Saved"
			return redirect("/register/")
		else:
			pass
			# print form.errors
		
	else:
		form = UserDetailsForm()

	context = {
	
		'form':form,
		'email_id':request.user.email
		
	}
	return render(request, 'test_site/register.html',context)



### MCQ View
@login_required(login_url='/login/',redirect_field_name=None)
def mcq_view(request):
	
	
	#### Resume session
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	
	#### Check if session has ended
	if current_session["ended"]:
		return redirect("/complete/")
	

	#### Get current status from session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	sec_id = sections_list[int(current_section)]
	if type(current_session["time_left"]) == type("asd"):
		current_session["time_left"] = json.loads(current_session["time_left"])
	time_left = current_session["time_left"][str(sec_id)]
	section = AssessmentSection.objects.get(pk=sec_id)

	if section.section_type == "code":
		return redirect("/oj/")
	elif section.section_type == "text":
		return redirect("/text/")

	question_id = default_session["sections"][sec_id][int(current_question)]
	question = MCQQuestions.objects.get(pk=question_id)
	# # print section.section_name
	question_text = question.question_body
	question_marks = question.marks

	if question.img_url is not None:
		img_url = question.img_url
	else:
		img_url = None

	if question.passage == True:
		passage_text = question.passage_obj.passage_body
		question_text = passage_text +"<br><br>"+question_text

	choices = MCQChoices.objects.filter(question=question)
	options_dict = {}
	for choice in choices:
		options_dict[str(choice.id)] = choice.choice_body
	timer_min = int(time_left)/60
	timer_sec = int(time_left)%60
	
	if MCQResponse.objects.filter(user=request.user,section=section,question=question).exists():
		
		try:
			user_response = MCQResponse.objects.get(user=request.user,section=section,question=question)
		except MultipleObjectsReturned:
			user_response = MCQResponse.objects.filter(user=request.user,section=section,question=question).first()
		
		if user_response.user_response is not None:
			selected_option = user_response.user_response.id
			# ques_attempted = True
		else:
			selected_option = None
	else:
		selected_option = None




	if request.method == 'POST':
		form = MCQForm(request.POST)
	
		if form.is_valid():
			if MCQResponse.objects.filter(user=request.user,section=section,question=question).exists():
				try:
					user_response = MCQResponse.objects.get(user=request.user,section=section,question=question)
				except MultipleObjectsReturned:
					user_response = MCQResponse.objects.filter(user=request.user,section=section,question=question).first()
				try:
					choice_selected = request.POST["options"]
					choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = choice
					selected_option = choice.id
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				
				user_response.save()
			else:
				user_response = MCQResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question = question
				selected_option = choice.id
				try:
					choice_selected = request.POST["options"]
					choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = choice
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				user_response.save()
			action_f = form.cleaned_data["bsource"]
			qno_next =form.cleaned_data["qno"]
			sno =form.cleaned_data["sno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])

			request.session, ended,section_change = updateCurrentSession(action_f,request,time_left,qno_next,ques_attempted,sno)
			if section_change == 1:
				return redirect("/section_instructions/")
			if ended:
				request.session["ended"] = True
				return redirect("/complete/")
			
			return redirect("/mcq/")

		
	
	form = MCQForm()
	ques_overview = section_overview["status"][sec_id]
	answered = 0
	unanswered = 0
	flagged = 0
	for q in ques_overview:
		if str(q["status"]) == "unanswered":
			unanswered += 1
		if str(q["status"]) == "answered":
			answered += 1
		if str(q["status"]) == "flagged":
			flagged += 1
	# print "Time Left: ",time_left
	context = {
	
		'form':form,
		'img_url':img_url,
		'options_dict':options_dict,
		'question_text':question_text,
		'timer_sec':timer_sec,
		'timer_min':timer_min,
		# 'timer_sec':-5,
		# 'timer_min':0,
		'test_name':current_session["assessment_name"],
		'section_name': section.section_name,
		'section_number': section.id,
		'ques_overview':section_overview["status"][sec_id],
		'current_ques':str(int(current_question)+1),
		'answered':answered,
		'unanswered':unanswered,
		'question_marks':question_marks,
		'flagged':flagged,
		'selected_option':selected_option,
		
	}
	# return render(request, 'test_site/mcq_obfs.html',context)
	return render(request, 'test_site/questionMCQ.html',context)

### Text ques view
# @login_required(login_url='/login/',redirect_field_name=None)
def text_view(request):
	
	# print "Here in text view"
	#### Resume session
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	
	#### Check if session has ended
	if current_session["ended"]:
		return redirect("/complete/")
	

	#### Get current status from session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	sec_id = sections_list[int(current_section)]
	if type(current_session["time_left"]) == type("asd"):
		current_session["time_left"] = json.loads(current_session["time_left"])
	time_left = current_session["time_left"][str(sec_id)]
	section = AssessmentSection.objects.get(pk=sec_id)

	if section.section_type == "mcq":
		return redirect("/mcq/")
	
	elif section.section_type == "code":
		return redirect("/oj/")

	question_id = default_session["sections"][sec_id][int(current_question)]
	question = TextQuestion.objects.get(pk=question_id)
	# # print section.section_name
	question_text = question.question_body
	question_marks = question.marks

	if question.img_url is not None:
		img_url = question.img_url
	else:
		img_url = None

	if question.passage == True:
		q_p = MCQPassage.objects.get(question = question)
		passage_text = q_p.passage_body
		question_text = passage_text +"\r\n"+question_text

	# choices = MCQChoices.objects.filter(question=question)
	# options_dict = {}
	# for choice in choices:
	# 	options_dict[str(choice.id)] = choice.choice_body
	timer_min = int(time_left)/60
	timer_sec = int(time_left)%60
	
	if TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).exists():
		
		try:
			user_response = TextQuestionResponse.objects.get(user=request.user,section=section,question=question)
		except MultipleObjectsReturned:
			user_response = TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).first()
		
		if user_response.user_response is not None:
			user_answer = user_response.user_response
			# ques_attempted = True
		else:
			user_answer = None
	else:
		user_answer = None




	if request.method == 'POST':
		form = TextForm(request.POST)
		print request.POST
	
		if form.is_valid():
			if TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).exists():
				try:
					user_response = TextQuestionResponse.objects.get(user=request.user,section=section,question=question)
				except MultipleObjectsReturned:
					user_response = TextQuestionResponse.objects.filter(user=request.user,section=section,question=question).first()
				try:
					user_answer = request.POST["user_response"]
					# choice = MCQChoices.objects.get(pk=choice_selected)
					user_response.user_response = user_answer
					# selected_option = choice.id
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					# selected_option = None
				
				user_response.save()
			else:
				user_response = TextQuestionResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question = question
				
				try:
					user_answer = request.POST["user_response"]
					user_response.user_response = user_answer
					ques_attempted = True
				except Exception as excp:
					user_response.user_response = None
					selected_option = None
				user_response.save()
			action_f = form.cleaned_data["bsource"]
			qno_next =form.cleaned_data["qno"]
			sno =form.cleaned_data["sno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])

			request.session, ended,section_change = updateCurrentSessionText(action_f,request,time_left,qno_next,ques_attempted,sno)
			section_change = 0
			ended = False
			if section_change == 1:
				return redirect("/section_instructions/")
			if ended:
				request.session["ended"] = True
				return redirect("/complete/")
			
			return redirect("/text/")

		
	
	form = TextForm()
	ques_overview = section_overview["status"][sec_id]
	answered = 0
	unanswered = 0
	flagged = 0
	for q in ques_overview:
		if str(q["status"]) == "unanswered":
			unanswered += 1
		if str(q["status"]) == "answered":
			answered += 1
		if str(q["status"]) == "flagged":
			flagged += 1
	print "Time Left: ",time_left
	if user_answer == None:
		user_answer = ""
	context = {
	
		'form':form,
		'img_url':img_url,
		# 'options_dict':options_dict,
		'question_text':question_text,
		'timer_sec':timer_sec,
		'timer_min':timer_min,
		# 'timer_sec':-5,
		# 'timer_min':0,
		'test_name':current_session["assessment_name"],
		'section_name': section.section_name,
		'section_number': section.id,
		'ques_overview':section_overview["status"][sec_id],
		'current_ques':str(int(current_question)+1),
		'answered':answered,
		'unanswered':unanswered,
		'question_marks':question_marks,
		'flagged':flagged,
		'user_answer':user_answer,
		
	}
	
	# return render(request, 'test_site/mcq_obfs.html',context)
	return render(request, 'test_site/questionText.html',context)




### OJ View

# @login_required(login_url='/login/',redirect_field_name=None)
def oj_view(request):
	
	#### Resume session
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	
	#### Check if session has ended
	if current_session["ended"]:
		return redirect("/complete/")
	

	#### Get current status from session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_question = current_session["current_question"]
	

	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	sec_id = sections_list[int(current_section)]
	
	if type(current_session["time_left"]) == type("asd"):
		current_session["time_left"] = json.loads(current_session["time_left"])
	

	section = AssessmentSection.objects.get(pk=sec_id)
	
	if section.section_type == "mcq":
		return redirect("/mcq/")

	elif section.section_type == "text":
		return redirect("/text/")

	section_name = section.section_name

	time_left = current_session["time_left"][str(sec_id)]
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = CodingQuestions.objects.get(pk=question_id)
	question_name = question.question_name
	question_text = question.question_body
	timer_min = int(time_left)/60
	timer_sec = int(time_left)%60
	form = OJForm()
	ques_overview = section_overview["status"][sec_id]
	answered = 0
	unanswered = 0
	flagged = 0
	for q in ques_overview:
		if str(q["status"]) == "unanswered":
			unanswered += 1
		elif str(q["status"]) == "answered":
			answered += 1
		elif str(q["status"]) == "flagged":
			flagged += 1

	if request.method == 'POST':
		form = OJForm(request.POST)
		if form.is_valid():
			print request.POST
			action_f = form.cleaned_data["bsource"]
			if CodingQuestionResponse.objects.filter(user=request.user,section=section,question_name=question_name).exists():
				user_response = CodingQuestionResponse.objects.get(user=request.user,section=section,question_name=question_name)
				user_response.code_body = form.cleaned_data["code_body"]
				user_response.code_language = form.cleaned_data["code_language"]
				# user_response.status = "Queued"
				if action_f == "bsub":
					user_response.submitted = True
					sub_id = "a"+str(uuid.uuid4()).replace("-","")
					user_response.unique_id = sub_id
				user_response.save()
				print "Updated DB"
			else:
				user_response = CodingQuestionResponse()
				user_response.user = request.user
				user_response.section = section
				user_response.question_name = question_name
				user_response.code_body = form.cleaned_data["code_body"]
				user_response.code_language = form.cleaned_data["code_language"]
				# user_response.status = "Queued"
				
				if action_f == "bsub":
					user_response.submitted = True
					sub_id = "a"+str(uuid.uuid4()).replace("-","")
					user_response.unique_id = sub_id
				user_response.save()
				print "Saved to DB"
			if action_f == "bsub":
				uploadCode(sub_id,question_name,form.cleaned_data["code_language"],form.cleaned_data["code_body"])

			
			if action_f == "bsub":
				ques_attempted = True
			qno_next =form.cleaned_data["qno"]
			timer_value = form.cleaned_data["timer_var"].split(":")
			time_left = int(timer_value[0])*60 + int(timer_value[1])

			request.session, ended,section_change = updateCurrentSessionOJ(action_f,request,time_left,qno_next,ques_attempted)

			if section_change == 1:
				return redirect("/section_instructions/")
				
			if ended:
				request.session["ended"] = True
				return redirect("/complete/")


		if action_f == "vsub":
			return redirect("/submissions/")	
		
		return redirect("/oj/")

	if CodingQuestionResponse.objects.filter(user=request.user,section=section,question_name=question_name).exists():
		print "Response found"
		user_response = CodingQuestionResponse.objects.get(user=request.user,section=section,question_name=question_name)
		existing_code = user_response.code_body
		code_lang = user_response.code_language
		if existing_code != "":
			ques_attempted = True
	else:
		existing_code =""
		code_lang = "Python 2.7"

		
	# print existing_code

	context = {
	
		'form':form,
		'question':question_text,
		'timer_min':timer_min,
		'timer_sec':timer_sec,
		# 'timer_min':0,
		# 'timer_sec':0,
		'ques_overview':ques_overview,
		'current_ques':str(int(current_question)+1),
		'test_name':current_session["assessment_name"],
		'section_name':section_name,
		'answered':answered,
		'unanswered':unanswered,
		'flagged':flagged,
		'existing_code':existing_code,
		'code_lang':code_lang,
		# 'email_id':"sample@sample.com"
		
	}


	return render(request, 'test_site/questionCoding.html',context)

### Feedback View
@login_required(login_url='/login/',redirect_field_name=None)
def feedback(request):
	errors = ""
	if request.method == 'POST':
		current_user = request.user
		if UserFeedback.objects.filter(user=current_user).exists():
			return redirect("/thankyou/")
		# print request.POST
		form = FeedbackForm(request.POST)
		if form.is_valid():
			new_feedback = UserFeedback()
			new_feedback.user =request.user
			new_feedback.q1 = form.cleaned_data['q1']
			new_feedback.q2 = form.cleaned_data['q2']
			new_feedback.q3 = form.cleaned_data['q3']
			new_feedback.q4 = form.cleaned_data['q4']
			new_feedback.q5 =form.cleaned_data['q5']
			new_feedback.save()
			return redirect("/thankyou/")
		else:
			pass
			# print form.errors
		
	else:
		form = FeedbackForm()

	context = {
	
		'form':form,
		
		
	}
	return render(request, 'test_site/feedback.html',context)


@login_required(login_url='/login/',redirect_field_name=None)
def thankyou(request):
	context={}
	request.session.flush()
	return render(request, 'test_site/thankyou.html',context)

@login_required(login_url='/login/',redirect_field_name=None)
def test_complete(request):
	context={}
	return render(request, 'test_site/thankyou_test.html',context)


@login_required(login_url='/login/',redirect_field_name=None)
def begin_test(request):
	
	context={
	

	}

	return render(request, 'test_site/begin_test.html',context)


@login_required(login_url='/login/',redirect_field_name=None)
def test_instructions(request):
	current_session = request.session["current_session"]
	assessment = Assessment.objects.get(test_name=current_session["assessment_name"])
	instructions = AssessmentInstruction.objects.filter(assessment=assessment).order_by('sequence')
	context={
	'instructions':instructions,
	'next':'/section_instructions/'

	}

	return render(request, 'test_site/test_instruction.html',context)

@login_required(login_url='/login/',redirect_field_name=None)
def section_instructions(request):
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	ended = current_session["ended"]
	if "ended" in current_session and current_session["ended"]:
		return redirect("/thankyou/")
	current_question = current_session["current_question"]
	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	# time_left = current_session["time_left"]
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	sec_id = sections_list[int(current_section)]
	section = AssessmentSection.objects.get(pk=sec_id)
	# current_session["time_left"] = int(section.section_duration)
	request.session["current_session"] = current_session
	# print section.section_name

	instructions = SectionInstruction.objects.order_by('sequence').filter(section=section)
	
	if section.section_type == "mcq":
		next_url = "/mcq/"
	elif section.section_type == "code":
		next_url = "/oj/"
	elif section.section_type == "memory-game":
		next_url = "/memory-game/"		
	elif section.section_type == "number-chronology":
		next_url = "/number-chronology/"		
	elif section.section_type == "moneyball":
		next_url = "/moneyball/"		
	elif section.section_type == "seven_up":
		next_url = "/7-up/"		


	context={
	'instructions':instructions,
	'next':next_url
	}
	return render(request, 'test_site/test_instruction.html',context)


@login_required(login_url='/login/',redirect_field_name=None)
def create_session(request,access_token=None):
	if access_token == None:
		current_user = request.user
		UPLOADED_TEST = SiteConfiguration.objects.get(config_key="uploaded_test")
		test_name = UPLOADED_TEST.config_value
		assessment_mod = Assessment.objects.get(test_name=test_name)
	else:
		pass
	if UserDefaultSession.objects.filter(user=current_user,assessment=assessment_mod).exists():
		new_session = UserDefaultSession.objects.get(user=current_user,assessment=assessment_mod)
		default_session = json.loads(new_session.default_state)
		# print "DEF Found"
	else:
		assessment = Assessment.objects.get(test_name=test_name)
		sections = AssessmentSection.objects.filter(assessment=assessment).order_by("section_name")
		for section in sections:
			pass
			# print section.section_name
		default_session = {}
		default_session["sections"] = {}
		for section in sections:
			if section.section_type == "mcq":
				section_id = section.id
				
				#Shuffle Non-Passage Question
				questions_wop = MCQQuestions.objects.filter(section=section,passage=False).order_by('id')
				question_list = []
				for question in questions_wop:
					question_list.append(question.id)
				# random.shuffle(question_list)
				ql = []
				#Do not shuffle Passage Question
				questions_wop = MCQQuestions.objects.filter(section=section,passage=True)
				for question in questions_wop:
					ql.append(question.id)
				in_p = random.randint(0,len(question_list))
				question_list[in_p:in_p] = ql
			
			elif section.section_type == "code":
				# print "Reached coding section"
				section_id = section.id
				questions_wop = CodingQuestions.objects.filter(section=section)
				question_list = []
				for question in questions_wop:
					print question.question_body

					question_list.append(question.id)
				# random.shuffle(question_list)
			elif section.section_type == "text":
				section_id = section.id
				
				#Shuffle Non-Passage Question
				questions_wop = TextQuestion.objects.filter(section=section,passage=False).order_by('id')
				question_list = []
				for question in questions_wop:
					question_list.append(question.id)
				# random.shuffle(question_list)
				ql = []
				#Do not shuffle Passage Question
				questions_wop = TextQuestion.objects.filter(section=section,passage=True)
				for question in questions_wop:
					ql.append(question.id)
				in_p = random.randint(0,len(question_list))
				question_list[in_p:in_p] = ql
			elif section.section_type == "memory-game":
				section_id = section.id
				question_list = [1]
			elif section.section_type == "number-chronology":
				section_id = section.id
				question_list = [1]
				
			default_session["sections"][section_id] = question_list
		# # print default_session
		new_session = UserDefaultSession()
		new_session.user = current_user
		new_session.assessment = assessment_mod
		new_session.default_state = json.dumps(default_session)
		new_session.save()

	request.session["default_session"] = default_session

	if UserSessionState.objects.filter(user=current_user,assessment=assessment_mod).exists():
		# print "CUR Found"
		current_session = UserSessionState.objects.get(user=current_user,assessment=assessment_mod)
		session_dict = {}
		session_dict["current_section"] = current_session.current_section
		session_dict["current_question"] = current_session.current_question
		# session_dict["test_name"] = current_session.test_name
		session_dict["assessment_name"] = assessment_mod.test_name
		session_dict["time_left"] = json.loads(current_session.time_left)
		session_dict["ended"] = current_session.ended
		session_dict["section_overview"] = json.loads(current_session.section_overview)
	else:
		current_session = UserSessionState()
		current_session.user = request.user
		current_session.assessment = assessment_mod
		current_session.current_section = 0
		current_session.current_question = 0
		current_session.ended = False
		time_dict = {}
		# current_session.time_left = {}
		sec_ids = sorted(default_session["sections"].keys())
		# print sec_ids
		for sec_id in sec_ids:
			sec_timer = AssessmentSection.objects.get(pk=sec_id)
			sec_timer = sec_timer.section_duration
			time_dict[str(sec_id)] = sec_timer
		current_session.time_left = json.dumps(time_dict)
		
		section_overview = {}
		section_overview["status"] = {}
		for sec_id in sec_ids:
			section_overview["status"][sec_id] = []
			ques_nos = default_session["sections"][sec_id]
			for i in range(1,len(ques_nos)+1):
				init1 = {}
				init1["qno"] = str(i)
				init1["status"] = "unanswered"
				section_overview["status"][sec_id].append(init1)
		current_session.section_overview = json.dumps(section_overview)
		current_session.save()
		session_dict = {}
		session_dict["current_section"] = current_session.current_section
		session_dict["current_question"] = current_session.current_question
		session_dict["time_left"] = current_session.time_left
		session_dict["ended"] = current_session.ended
		session_dict["assessment_name"] = assessment_mod.test_name
		session_dict["section_overview"] = json.loads(current_session.section_overview)

	request.session["current_session"] = session_dict
	# print session_dict
	# print default_session

	return redirect("/test_instructions/")

	


@login_required(login_url='/login/',redirect_field_name=None)
def oj_subs(request):
	
	#### Resume session
	ques_attempted = False
	default_session = request.session["default_session"]
	current_session = request.session["current_session"]
	
	#### Check if session has ended
	if current_session["ended"]:
		return redirect("/complete/")
	

	#### Get current status from session
	sections_list = sorted(default_session["sections"].keys(),reverse=False)
	current_question = current_session["current_question"]
	

	current_section = current_session["current_section"]
	section_overview = current_session["section_overview"]
	sec_id = sections_list[int(current_section)]
	
	if type(current_session["time_left"]) == type("asd"):
		current_session["time_left"] = json.loads(current_session["time_left"])
	

	section = AssessmentSection.objects.get(pk=sec_id)
	
	if section.section_type == "mcq":
		return redirect("/mcq/")
	elif section.section_type == "text":
		return redirect("/text/")	

	section_name = section.section_name

	time_left = current_session["time_left"][str(sec_id)]
	# time_left = 900
	question_id = default_session["sections"][sec_id][int(current_question)]
	question = CodingQuestions.objects.get(pk=question_id)
	question_name = question.question_name
	question_text = question.question_body

	# question_name = "Sample Question Name"
	# question_text = "Sample Question Body"

	timer_min = int(time_left)/60
	timer_sec = int(time_left)%60
		

	form = OJForm()
	ques_overview = section_overview["status"][sec_id]
	answered = 0
	unanswered = 0
	flagged = 0
	for q in ques_overview:
		if str(q["status"]) == "unanswered":
			unanswered += 1
		elif str(q["status"]) == "answered":
			answered += 1
		elif str(q["status"]) == "flagged":
			flagged += 1

	if request.method == 'POST':
		form = OJForm(request.POST)
		if form.is_valid():
			print request.POST
			action_f = form.cleaned_data["bsource"]
			qno_next =form.cleaned_data["qno"]
			request.session, ended,section_change = updateCurrentSessionOJ(action_f,request,time_left,qno_next,ques_attempted)

			if section_change == 1:
				return redirect("/section_instructions/")
				
			if ended:
				request.session["ended"] = True
				return redirect("/complete/")


			if action_f == "vsub":
				return redirect("/submissions/")	
		else:
			print form.errors
		return redirect("/oj/")

	submissions = CodingQuestionResponse.objects.filter(user=request.user,section=section,submitted=True)
	context = {
	
		'form':form,
		'question':question_text,
		'timer_min':timer_min,
		'timer_sec':timer_sec,
		# 'timer_min':0,
		# 'timer_sec':0,
		'ques_overview':ques_overview,
		'current_ques':str(int(current_question)+1),
		'test_name':current_session["assessment_name"],
		'section_name':section_name,
		'answered':answered,
		'unanswered':unanswered,
		'flagged':flagged,
		'submissions':submissions,
		# 'email_id':"sample@sample.com"
		
	}


	return render(request, 'test_site/questionSubs.html',context)


def seven_up_sample(request):

	if request.method == 'POST':
		print request.POST
		message_sent = json.dumps(request.POST)
		return HttpResponse(message_sent)
	context = {}
	return render(request, 'test_site/7up.html',context)

def moneyball_sample(request):

	if request.method == 'POST':
		print request.POST
		message_sent = json.dumps(request.POST)
		return HttpResponse(message_sent)

	context = {}
	return render(request, 'test_site/moneyball.html',context)

def memory_game_sample(request):
	
	config_obj = GameConfiguration.objects.get(game_id="memory-game")
	context = {}
	context["grid_size"] = config_obj.grid_rows
	context["flash_time"] = config_obj.flash_time
	context["num_lives"] = config_obj.num_lives
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	context["input_persistance_time"] = config_obj.input_persistance_time
	context["persist_wrong_input"] = config_obj.persist_wrong_input
	context["persist_until_input"] = config_obj.persist_until_input





	if request.method == 'POST':
		print request.POST
		message_sent = json.dumps(request.POST)
		return HttpResponse(message_sent)

	
	return render(request, 'test_site/memory-game.html',context)



@login_required(login_url='/login/',redirect_field_name=None)
def memory_game(request):
	
	config_obj = GameConfiguration.objects.get(game_id="memory-game")
	context = {}
	context["grid_size"] = config_obj.grid_rows
	context["flash_time"] = config_obj.flash_time
	context["num_lives"] = config_obj.num_lives
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	context["input_persistance_time"] = config_obj.input_persistance_time
	context["persist_wrong_input"] = config_obj.persist_wrong_input
	context["persist_until_input"] = config_obj.persist_until_input





	if request.method == 'POST':
		print request.POST
		error_logs = json.loads(request.POST["error_log"])
		score = request.POST["score"]
		lives = request.POST["num_lives"]
		game_result = GameResult()
		game_result.user = request.user
		game_result.score = int(score)
		game_result.lives = int(lives)
		game_result.time_stamp = timezone.now()
		game_result.game = config_obj
		game_result.save()

		for error_log in error_logs:
			game_log = GameLog()
			game_log.game_session = game_result
			game_log.level = int(error_log["level"])
			game_log.lives = int(error_log["lives"])
			game_log.grid_x = int(error_log["gridSize"])
			game_log.grid_y = int(error_log["gridSize"])
			game_log.score = int(error_log["score"])
			game_log.event = error_log["event"]
			game_log.input_name = str(error_log["inputName"])
			game_log.expected = error_log["expected"]
			game_log.time_elapsed = error_log["timeElaspsed"]
			game_log.save()

		request.session,ended,section_change = updateCurrentSessionGame(request)

		if ended:
			return redirect("/complete/")
		else:
			return redirect("/section_instructions/")

	
	return render(request, 'test_site/memory-game.html',context)

@login_required(login_url='/login/',redirect_field_name=None)
def number_chronology(request):
	
	config_obj = GameConfiguration.objects.get(game_id="number-chronology")
	context = {}
	context["grid_rows"] = config_obj.grid_rows
	context["grid_cols"] = config_obj.grid_cols
	context["flash_time"] = config_obj.flash_time
	context["num_lives"] = config_obj.num_lives
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	context["input_persistance_time"] = config_obj.input_persistance_time
	context["persist_wrong_input"] = config_obj.persist_wrong_input
	context["persist_until_input"] = config_obj.persist_until_input





	if request.method == 'POST':
		print request.POST
		error_logs = json.loads(request.POST["error_log"])
		score = request.POST["score"]
		lives = request.POST["num_lives"]
		game_result = GameResult()
		game_result.user = request.user
		game_result.score = int(score)
		game_result.lives = int(lives)
		game_result.time_stamp = timezone.now()
		game_result.game = config_obj
		game_result.save()

		for error_log in error_logs:
			game_log = GameLog()
			game_log.game_session = game_result
			game_log.level = int(error_log["level"])
			game_log.lives = int(error_log["lives"])
			game_log.grid_x = int(error_log["gridX"])
			game_log.grid_y = int(error_log["gridY"])
			game_log.score = int(error_log["score"])
			game_log.event = error_log["event"]
			game_log.input_name = str(error_log["inputName"])
			game_log.expected = error_log["expected"]
			game_log.time_elapsed = error_log["timeElaspsed"]
			game_log.save()

		request.session,ended,section_change = updateCurrentSessionGame(request)

		if ended:
			return redirect("/feedback/")
		else:
			return redirect("/section_instructions/")

	
	return render(request, 'test_site/number-chronology.html',context)

@login_required(login_url='/login/',redirect_field_name=None)
def seven_up(request):
	
	config_obj = GameConfiguration.objects.get(game_id="seven_up")
	context = {}
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	




	if request.method == 'POST':
		print request.POST
		error_logs = json.loads(request.POST["error_log"])
		score = request.POST["score"]
		lives = request.POST["num_lives"]
		game_result = GameResult()
		game_result.user = request.user
		game_result.score = 0
		game_result.lives = 0
		game_result.time_stamp = timezone.now()
		game_result.game = config_obj
		game_result.save()
		# print "======="
		# print error_logs
		# # error_logs = error_logs[0]
		# print "======="
		# # print error_logs

		for error_log in error_logs:
			print error_log
			game_log = SevenUpLog()
			game_log.game_session = game_result
			game_log.level = int(error_log["level"])
			game_log.event = error_log["event"]
			game_log.time_elapsed = error_log["timeElaspsed"]
			game_log.depsoited_7up = int(error_log["deposites"][0])
			game_log.depsoited_7 = int(error_log["deposites"][1])
			game_log.depsoited_7down = int(error_log["deposites"][2])
			if "diceValues" in error_log:
				game_log.dice1_value = int(error_log["diceValues"][0])
				game_log.dice2_value = int(error_log["diceValues"][1])
			else:
				game_log.dice1_value = 0
				game_log.dice2_value = 0
			game_log.profit_7up = int(error_log["profits"][0])
			game_log.profit_7 = int(error_log["profits"][1])
			game_log.profit_7down = int(error_log["profits"][2])
			game_log.save()
			print "Saved"

		request.session,ended,section_change = updateCurrentSessionGame(request)

		if ended:
			return redirect("/feedback/")
		else:
			return redirect("/section_instructions/")

	
	return render(request, 'test_site/7up.html',context)

@login_required(login_url='/login/',redirect_field_name=None)
def moneyball(request):
	
	config_obj = GameConfiguration.objects.get(game_id="moneyball")
	context = {}
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	




	if request.method == 'POST':
		print request.POST
		error_logs = json.loads(request.POST["error_log"])
		# score = request.POST["score"]
		# lives = request.POST["num_lives"]
		game_result = GameResult()
		game_result.user = request.user
		game_result.score = 0
		game_result.lives = 0
		game_result.time_stamp = timezone.now()
		game_result.game = config_obj
		game_result.save()
		# print "======="
		# print error_logs
		# # error_logs = error_logs[0]
		# print "======="
		# # print error_logs

		for error_log in error_logs:
			print error_log
			game_log = MoneyballLog()
			game_log.game_session = game_result
			game_log.level = int(error_log["level"])
			game_log.event = error_log["event"]
			game_log.time_elapsed = error_log["timeElaspsed"]
			game_log.save()
			actions = error_log["actions"]
			for action in actions:
				action_obj = MoneyballAction()
				action_obj = game_log
				action_obj.amount = int(action["amount"])
				action_obj.market = action["market"]
				action_obj.action = action["amount"]
				action_obj.save()
		# 	
		# 	print "Saved"

		request.session,ended,section_change = updateCurrentSessionGame(request)

		if ended:
			return redirect("/feedback/")
		else:
			return redirect("/section_instructions/")

	
	return render(request, 'test_site/moneyball.html',context)


def number_chronology_sample(request):

	config_obj = GameConfiguration.objects.get(game_id="number-chronology")
	context = {}
	context["grid_rows"] = config_obj.grid_rows
	context["grid_cols"] = config_obj.grid_cols
	context["flash_time"] = config_obj.flash_time
	context["num_lives"] = config_obj.num_lives
	context["timeout"] = config_obj.timeout
	context["level_transition_time"] = config_obj.level_transition_time
	context["input_persistance_time"] = config_obj.input_persistance_time
	context["persist_wrong_input"] = config_obj.persist_wrong_input
	context["persist_until_input"] = config_obj.persist_until_input


	if request.method == 'POST':
		print request.POST
		message_sent = json.dumps(request.POST)
		return HttpResponse(message_sent)
	
	
	return render(request, 'test_site/number-chronology.html',context)

def upload(request):
	assessment = Assessment.objects.get(test_name="Futures First")
	print assessment.id
	section = AssessmentSection.objects.get(assessment=assessment,section_name="Futures First - Section 2")
	questions = MCQQuestions.objects.filter(section=section)
	count = 0
	# for ques in questions:
	# 	# if ques.tag1 == "LR":
	# 	# 	ques.tag1 = "LR and DI"
	# 	# elif ques.tag1 == "Quants":
	# 	# 	ques.tag1 = "Quantitative Aptitude"
	# 	# elif ques.tag1 == "DI":
	# 	# 	ques.tag1 = "LR and DI"
	# 	ques.negative_marks = -1/3
	# 	ques.save()
	# 	count +=1
	# 	print count
	# 	# print ques.tag1


	# print section.id
	# # for section in sections:
	# 	# pass
	# 	# print section.section_name, section.id
	import pandas as pd
	# import numpy as np

	excel_file = "/Users/abm17/Cantilever/BITS_Analytics_1/FF/colleges.xlsx"

	df = pd.read_excel(excel_file,sheet_name = 1)
	for index, row in df.iterrows():
		print """<li class="mdl-menu__item" data-val="%s">%s</li>""" %(row["Degree List"],row["Degree List"])
	return HttpResponse("Printed")
	# columns = df.columns
	# print columns
	# option_columns = columns[3:8]

	# print option_columns

	# section_obj = section



	# for index, row in df.iterrows():
	# 	# try:
	# 	correct_option = "Option "+str(int(row["RO"]))
	# 	ques_obj = MCQQuestions()
	# 	ques_obj.question_body = row["Question"]
	# 	# if row["Passage"] != "No Passage":
	# 	# 	ques_obj.question_body = row["Passage"] + "<BR><BR>" + row["Question"]
	# 	ques_obj.section = section_obj
	# 	ques_obj.marks= int(row["Marks"])
	# 	ques_obj.negative_marks = 0
	# 	ques_obj.tag1=row["Topic"]
	# 	ques_obj.tag2=row["Difficulty"]
	# 	ques_obj.passage = False
		
	# 	if str(row["Image"]) != "No Image":
	# 		# try:
	# 		s3 = "https://s3.ap-south-1.amazonaws.com/question-images-cantliever/ff-new/"
	# 		print row["Image"]
			
	# 		ques_obj.img_url = s3 + str(row["Image"])  +".jpg"
	# 		print ques_obj.img_url
	# 		# except Exception as excp:
	# 			# print "Error in : ",index + 1
		
	# 	ques_obj.save()
	# 	print "Question created: ", index

	# 	print row["Question"]
		
	# 	for col in option_columns:
	# 		print col, correct_option, col==correct_option
	# 		if row[col] != "No Option":
	# 			choice_obj = MCQChoices()
	# 			choice_obj.choice_body = row[col]
	# 			if str(col) == str(correct_option):
	# 				print "Found Correct"
	# 				print index
	# 				choice_obj.correct_flag = True
	# 			else:
	# 				choice_obj.correct_flag = False
	# 			choice_obj.question = ques_obj
	# 			choice_obj.save()
	# 				# print "choice created:", col
	# 				# print row[col]
	# 	# except Exception as excp:
	# 	# 	print "Panga"
	# 	# 	print excp
	# 	# 	# print row["Question"]
	# # 	# 	continue
	
	# return HttpResponse("Questions Uploaded")


def addUser(row):
	username =  row["Username"]
	print username
	return username

def createUser(row):
	user_name = str(row["Username"])
	password = ''.join(random.choice(string.ascii_uppercase) for _ in range(8))
	# password = str(row["Password"])
	user=User.objects.create_user(user_name, password=password)
	user.email = user_name + "@email.com"
	# user.email = row["Email ID"]
	user.save()
	print password
	return password
	


def delete(request):
	# Flush all users
	# users_all = User.objects.all()
	# for user_one in users_all:
	# 	if str(user_one.username).startswith("merilytics"):
	# 		user_one.is_active = False
	# 		user_one.save()
	# 		print "Enabled: ", user_one.username
	# 	# elif str(user_one.username) == "abhijeet":
	# 	# 	pass
	# 	# else:
	# 	# 	user_one.is_active = False
	# 	# 	user_one.save()
	# 	# 	print "disabled: ", user_one.username
	# return HttpResponse("Users Updated")
	
	# Delete all users
	users_all = User.objects.filter(username__contains= "load_bot").delete()


	# for user_one in users_all:
	# 	if str(user_one.username).startswith("load_bot"):
	# 		# user_one.is_active = False
	# 		# user_one.save()
	# 		user_one.delete()
	# 		print "Deleted: ", user_one.username
		# elif str(user_one.username) == "abhijeet":
		# 	pass
		# else:
		# 	user_one.is_active = False
		# 	user_one.save()
		# 	print "disabled: ", user_one.username
	return HttpResponse("Load Users Deleted")

	# Generic users
	# sets = ["frugaltesting"]
	# for set_one in sets:
	# 	for i in range(1,11):
	# 		try:
	# 			user_name = "%s_%s@email.com" %(set_one,str(i))
	# 			print user_name
	# 			user=User.objects.create_user(user_name, password='password4321')
	# 			user.email = user_name
	# 			user.save()
	# 		except Exception as excp:
	# 			print excp
	# 			continue
	# return HttpResponse("Users Created")

	# From user list
	# import pandas as pd

	# excel_file = "/Users/abm17/Cantilever/BITS_Analytics_1/PracticeTestApplicants.xlsx"
	# df = pd.read_excel(excel_file,sheet_name = 2)
	
	# # df = df.drop_duplicates(["Roll No"])
	# # df["Username"] = df.apply(lambda row: addUser(row),axis=1)
	# # try:
	# df["Password"] = df.apply(lambda row: createUser(row),axis=1)
	# df.to_csv("pt_bitshyd_users_extra.csv",index=False)
	

	# return HttpResponse("Users Created")

def create(request):
	usernames = []
# 	for i in range(1,301):
# 		usernames.append("game_sample%d@email.com" %(i))
# 	User.objects.bulk_create([
# 	User(
# 		username=name,
# 		email=name,
# 		password=make_password('password4321',None, 'md5'),
# 		is_active=True,
# 	) for name in usernames
# ])
# 	return HttpResponse("Load Users Created")

	# Flush all users
	# users_all = User.objects.all()
	# for user_one in users_all:
	# 	if str(user_one.username).startswith("merilytics"):
	# 		user_one.is_active = False
	# 		user_one.save()
	# 		print "Enabled: ", user_one.username
	# 	# elif str(user_one.username) == "abhijeet":
	# 	# 	pass
	# 	# else:
	# 	# 	user_one.is_active = False
	# 	# 	user_one.save()
	# 	# 	print "disabled: ", user_one.username
	# return HttpResponse("Users Updated")
	
	# Delete all users
	# users_all = User.objects.filter(username__contains= "load_bot").delete()


	# for user_one in users_all:
	# 	if str(user_one.username).startswith("load_bot"):
	# 		# user_one.is_active = False
	# 		# user_one.save()
	# 		user_one.delete()
	# 		print "Deleted: ", user_one.username
		# elif str(user_one.username) == "abhijeet":
		# 	pass
		# else:
		# 	user_one.is_active = False
		# 	user_one.save()
		# 	print "disabled: ", user_one.username
	# return HttpResponse("Users Updated")

	# Generic users
	# sets = ["frugaltesting"]
	# for set_one in sets:
	# 	for i in range(1,11):
	# 		try:
	# 			user_name = "%s_%s@email.com" %(set_one,str(i))
	# 			print user_name
	# 			user=User.objects.create_user(user_name, password='password4321')
	# 			user.email = user_name
	# 			user.save()
	# 		except Exception as excp:
	# 			print excp
	# 			continue
	# return HttpResponse("Users Created")

	# From user list
	import pandas as pd

	excel_file = "/Users/abm17/Cantilever/cl/ff_onsite/ff_280319.xlsx"
	df = pd.read_excel(excel_file,sheet_name = 0)
	
	# df = df.drop_duplicates(["Roll No"])
	df["Username"] = df.apply(lambda row: addUser(row),axis=1)
	# try:
	df["Password"] = df.apply(lambda row: createUser(row),axis=1)
	df.to_csv("ff_280319_users.csv",index=False)
	

	return HttpResponse("Users Created")
