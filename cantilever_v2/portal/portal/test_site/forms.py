from django import forms

choices_topics = [("Select a topic","Select a topic"),("Quants","Quants"),("Logical Reasoning","Logical Reasoning"),("Data Interpretation","Data Interpretation"),("Verbal Ability","Verbal Ability")]




class LoginForm(forms.Form):
	user_id = forms.CharField(label="User Id", max_length=100,widget=forms.TextInput(attrs={'class':'mdl-textfield__input'}))
	password = forms.CharField(label="Password: ",widget=forms.PasswordInput(attrs={'class':'mdl-textfield__input'}),max_length=100)
	session_key = forms.CharField(label="Session Key: ",widget=forms.PasswordInput(attrs={'class':'mdl-textfield__input'}),max_length=100)
	
class UserDetailsForm(forms.Form):
	name = forms.CharField(label="Name",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'Name'}))
	InputCollegeName = forms.CharField(label="College Name",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'InputCollegeName'}))
	InputBranch = forms.CharField(label="Specialisation ",widget=forms.TextInput(attrs={'class':'mdl-textfield__input', 'id':'InputBranch'}))
	# InputCollegeName = forms.CharField(label="College Name",max_length=200,widget=forms.HiddenInput())
	# InputBranch = forms.CharField(label="Branch",max_length=200,widget=forms.HiddenInput())
	student_id = forms.CharField(label="Student ID",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'InputRollNumber'}))
	
	home_town = forms.CharField(label="Home Town",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'home_town'}))
	home_state = forms.CharField(label="Home State",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'home_state'}))
	work_experience = forms.CharField(label="Work Experience",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'work_experience'}))
	years_of_experience = forms.IntegerField(label="Years of Experience",min_value=0,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'years_of_experience'}))
	org_name = forms.CharField(label="Current/Last Organisation",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'org_name'}))
	

	user_email = forms.EmailField(label="Email ID",max_length=200,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','id':'InputRollNumber'}))
	cgpa = forms.DecimalField(label="CGPA",max_digits=4, decimal_places=2,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','pattern':'-?[0-9]*(\.[0-9]+)?', 'id':'cgpa'}))
	marks_12 = forms.DecimalField(label="10th Marks",max_digits=5, decimal_places=2,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','pattern':'-?[0-9]*(\.[0-9]+)?', 'id':'cgpa'}))
	marks_10 = forms.DecimalField(label="12th Marks",max_digits=5, decimal_places=2,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','pattern':'-?[0-9]*(\.[0-9]+)?', 'id':'cgpa'}))
	mobile = forms.CharField(label="Mobile Number",max_length=10,widget=forms.TextInput(attrs={'class':'mdl-textfield__input','pattern':'-?[0-9]*(\.[0-9]+)?', 'id':'mobile'}))
	expected_year = forms.DateField(label="Expected Year of Graduation (YYYY)",input_formats=['%Y'],widget=forms.DateInput(attrs={'class':'mdl-textfield__input','id':'InputYOG','pattern':'-?[0-9]*(\.[0-9]+)?',}))
	dob = forms.DateField(label="DOB (DD/MM/YYYY)",input_formats=['%d/%m/%Y'],widget=forms.DateInput(attrs={'class':'mdl-textfield__input','id':'InputDOB'}))


class FeedbackForm(forms.Form):
	q1 = forms.DecimalField(max_digits=4, decimal_places=2,widget=forms.HiddenInput(attrs={'id':'q1'}))
	q2 = forms.DecimalField(max_digits=4, decimal_places=2,widget=forms.HiddenInput(attrs={'id':'q2'}))
	q3 = forms.DecimalField(max_digits=4, decimal_places=2,widget=forms.HiddenInput(attrs={'id':'q3'}))
	q4 = forms.ChoiceField(choices = choices_topics,widget=forms.Select())
	q5 = forms.CharField(widget=forms.NumberInput())

class OJForm(forms.Form):
	bsource = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'bsource'}))
	code_language = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'code_language'}))
	code_body = forms.CharField(required=False,max_length=50000,widget=forms.HiddenInput(attrs={'id':'code_body'}))
	timer_var = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'timer_var'}))
	qno = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'qno'}))

class TextForm(forms.Form):
	bsource = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'bsource'}))
	timer_var = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'timer_var'}))
	qno = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'qno'}))
	sno = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'sno'}))
	user_response = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'user_response'}))



class MCQForm(forms.Form):
	bsource = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'bsource'}))
	timer_var = forms.CharField(max_length=50,widget=forms.HiddenInput(attrs={'id':'timer_var'}))
	qno = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'qno'}))
	sno = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'sno'}))
	option_sel = forms.CharField(required=False,max_length=50,widget=forms.HiddenInput(attrs={'id':'option_sel'}))
	