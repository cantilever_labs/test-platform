import uuid
import boto3
import os


def uploadCode(sub_id,question_name,code_language,code_body):
	code_ext = {
	"Python 2.7":"py",
	"C":"c",
	"C++":"cpp",
	"JavaScript":"js",
	"Java":"java",
	}
	if code_language == "Python 2.7" or code_language == "JavaScript":
		file_ext = code_ext[code_language]
		file_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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",region_name="ap-south-1")
		s3.Bucket('cantilever-user-codes').put_object(Key=file_name, Body=code_body)

	elif code_language == "C":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		compiled_file = "%s_%s.co" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("gcc -o %s %s" %(compiled_file,source_name))
		if ccheck == 0:
			# os.system("chmod u+x %s" %(compiled_file))
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")
			

	elif code_language == "C++":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_name = "%s_%s.%s" %(sub_id,question_name,file_ext)
		compiled_file = "%s_%s.cppo" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("g++ -o %s %s" %(compiled_file,source_name))
		if ccheck == 0:
			# os.system("chmod u+x %s" %(compiled_file))
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")

	elif code_language == "Java":
		file_ext = code_ext[code_language]
		source_code = code_body
		source_code = source_code.replace("SolutionClass", sub_id)
		source_name = "%s.%s" %(sub_id,file_ext)
		compiled_file = "%s.class" %(sub_id,question_name)
		file_object  = open(source_name, "w")
		file_object.write(source_code)
		file_object.close()
		ccheck = os.system("javac %s" %(source_name))
		if ccheck == 0:
			file_object  = open(compiled_file, "rb")
			compiled_code = file_object.read()
			file_object.close()
			s3 = boto3.resource('s3',aws_access_key_id="AKIAJK3VUVALAX63QMEQ",aws_secret_access_key="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")
			s3.Bucket('cantilever-user-codes').put_object(Key=compiled_file, Body=compiled_code)
			os.system("rm %s %s" %(source_name,compiled_file))
			updateDB(sub_id,"Queued")
		else:
			updateDB(sub_id,"Wrong Answer")

uploadCode("asd","helloworld","Python 2.7","""print "Hello World" """)