# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-07 01:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('test_site', '0007_auto_20180627_0651'),
    ]

    operations = [
        migrations.AddField(
            model_name='codingquestionresponse',
            name='submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='codingquestionresponse',
            name='score',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
