# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-18 08:36
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Assessment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_name', models.CharField(max_length=200, unique=True)),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='AssessmentInstruction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('instruction', models.CharField(max_length=2000)),
                ('assessment', models.ManyToManyField(to='test_site.Assessment')),
            ],
        ),
        migrations.CreateModel(
            name='AssessmentSection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('section_name', models.CharField(max_length=2000)),
                ('section_duration', models.PositiveIntegerField()),
                ('section_type', models.CharField(default='mcq', max_length=2000)),
                ('assessment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='test_site.Assessment')),
            ],
        ),
        migrations.CreateModel(
            name='CodingQuestionResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code_body', models.CharField(max_length=20000, null=True)),
                ('code_language', models.CharField(max_length=200)),
                ('question_name', models.CharField(max_length=200)),
                ('score', models.PositiveIntegerField()),
                ('status', models.CharField(max_length=100)),
                ('unique_id', models.CharField(max_length=100)),
                ('section', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.AssessmentSection')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CodingQuestions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_body', models.CharField(max_length=20000)),
                ('question_name', models.CharField(max_length=200, unique=True)),
                ('marks', models.PositiveIntegerField()),
                ('section', models.ManyToManyField(to='test_site.AssessmentSection')),
            ],
        ),
        migrations.CreateModel(
            name='MCQChoices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('choice_body', models.CharField(max_length=200)),
                ('correct_flag', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='MCQPassage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('passage_body', models.CharField(max_length=2000)),
            ],
        ),
        migrations.CreateModel(
            name='MCQQuestions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_body', models.CharField(max_length=2000)),
                ('img_url', models.URLField(blank=True, null=True)),
                ('marks', models.PositiveIntegerField()),
                ('passage', models.BooleanField()),
                ('passage_f', models.PositiveIntegerField(null=True)),
                ('section', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.AssessmentSection')),
            ],
        ),
        migrations.CreateModel(
            name='MCQResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='test_site.MCQQuestions')),
                ('section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='test_site.AssessmentSection')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('user_response', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.MCQChoices')),
            ],
        ),
        migrations.CreateModel(
            name='SectionInstruction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('instruction', models.CharField(max_length=2000)),
                ('section', models.ManyToManyField(to='test_site.AssessmentSection')),
            ],
        ),
        migrations.CreateModel(
            name='UserDefaultSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('default_state', models.CharField(max_length=20000)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserFeedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('q4', models.CharField(max_length=200)),
                ('q5', models.CharField(max_length=200)),
                ('q1', models.DecimalField(decimal_places=2, max_digits=4)),
                ('q2', models.DecimalField(decimal_places=2, max_digits=4)),
                ('q3', models.DecimalField(decimal_places=2, max_digits=4)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('college_name', models.CharField(max_length=200)),
                ('branch', models.CharField(max_length=200)),
                ('student_id', models.CharField(max_length=200)),
                ('cgpa', models.DecimalField(decimal_places=2, max_digits=4)),
                ('mobile', models.CharField(max_length=10)),
                ('expected_year', models.DateField()),
                ('dob', models.DateField()),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserSessionState',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('current_section', models.CharField(max_length=20000)),
                ('current_question', models.CharField(max_length=20000)),
                ('time_left', models.PositiveIntegerField()),
                ('section_overview', models.CharField(max_length=20000)),
                ('ended', models.BooleanField(default=False)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='mcqpassage',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='test_site.MCQQuestions'),
        ),
        migrations.AddField(
            model_name='mcqchoices',
            name='question',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='test_site.MCQQuestions'),
        ),
    ]
