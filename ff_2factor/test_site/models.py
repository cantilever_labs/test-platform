# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
import random,string

class SessionKey(models.Model):
    session_key = models.CharField(max_length=200,unique=True, default=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(12)))
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    def __unicode__(self):
       return self.session_key



class UserProfile(models.Model):
    user = models.OneToOneField(User)
    college_name = models.CharField(max_length=200)
    branch = models.CharField(max_length=200)
    student_id = models.CharField(max_length=200)
    cgpa = models.DecimalField(max_digits=4, decimal_places=2) 
    mobile = models.CharField(max_length=10)
    expected_year = models.DateField()
    dob = models.DateField()

    def __unicode__(self):
       return self.user.first_name

class UserFeedback(models.Model):
    user = models.OneToOneField(User)
    q4 = models.CharField(max_length=200)
    q5 = models.CharField(max_length=200)
    q1 = models.DecimalField(max_digits=4, decimal_places=2) 
    q2 = models.DecimalField(max_digits=4, decimal_places=2) 
    q3 = models.DecimalField(max_digits=4, decimal_places=2) 

    def __unicode__(self):
       return self.user.first_name

    

class Assessment(models.Model):
    test_name = models.CharField(max_length=200,unique=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    def __unicode__(self):
       return self.test_name

class AssessmentSection(models.Model):
    section_name = models.CharField(max_length=2000)
    assessment = models.ForeignKey(Assessment)
    section_duration = models.PositiveIntegerField()
    section_type = models.CharField(max_length=2000, default="mcq")

    def __unicode__(self):
       return self.section_name

class MCQQuestions(models.Model):
    question_body = models.CharField(max_length=2000)
    img_url = models.URLField(blank=True,null=True)
    section = models.ForeignKey(AssessmentSection,null=True)
    marks = models.PositiveIntegerField()
    negative_marks = models.FloatField(default=0)
    passage = models.BooleanField()
    # passage_f= models.PositiveIntegerField(null=True,blank=True)
    tag1 = models.CharField(max_length=2000,null=True,blank=True) # Topic
    tag2 = models.CharField(max_length=2000,null=True,blank=True) # Difficulty
    tag3 = models.CharField(max_length=2000,null=True,blank=True)
    tag4 = models.CharField(max_length=2000,null=True,blank=True)
    tag5 = models.CharField(max_length=2000,null=True,blank=True)

    def __unicode__(self):
       return self.question_body

class MCQPassage(models.Model):
    passage_body = models.CharField(max_length=3000)
    question = models.ManyToManyField(MCQQuestions)

    def __unicode__(self):
       return self.passage_body



class MCQChoices(models.Model):
    choice_body = models.CharField(max_length=200)
    question = models.ForeignKey(MCQQuestions,null=True)
    correct_flag = models.BooleanField()

    def __unicode__(self):
       return self.choice_body


class CodingQuestions(models.Model):
    question_body = models.CharField(max_length=20000)
    question_name = models.CharField(max_length=200,unique=True)
    section = models.ManyToManyField(AssessmentSection)
    marks = models.PositiveIntegerField()
    memory_limit = models.PositiveIntegerField(default=0)
    time_limit = models.PositiveIntegerField(default=0)

    def __unicode__(self):
       return self.question_name

class UserDefaultSession(models.Model):
    user = models.OneToOneField(User)
    default_state = models.CharField(max_length=20000)

    def __unicode__(self):
       return self.user.username

class UserActive(models.Model):
    user = models.OneToOneField(User)
    logged_in = models.BooleanField(default=False)


class UserSessionState(models.Model):
    user = models.OneToOneField(User)
    current_section = models.CharField(max_length=20000)
    current_question = models.CharField(max_length=20000)
    time_left = models.CharField(max_length=200)
    section_overview = models.CharField(max_length=20000)
    ended =models.BooleanField(default=False)

    def __unicode__(self):
       return self.user.username

class AssessmentInstruction(models.Model):
    assessment = models.ManyToManyField(Assessment)
    instruction = models.CharField(max_length=2000)
    sequence = models.PositiveIntegerField(default=1)

    def __unicode__(self):
       return self.instruction

class SectionInstruction(models.Model):
    section = models.ManyToManyField(AssessmentSection)
    instruction = models.CharField(max_length=2000)
    sequence = models.PositiveIntegerField(default=1)

    def __unicode__(self):
       return self.instruction
    


class MCQResponse(models.Model):
    user = models.ForeignKey(User)
    section = models.ForeignKey(AssessmentSection)
    question = models.ForeignKey(MCQQuestions)
    user_response = models.ForeignKey(MCQChoices,null=True)

    def __unicode__(self):
       return self.user.username
    
class CodingQuestionResponse(models.Model):
    user = models.ForeignKey(User)
    code_body = models.CharField(max_length=20000,null=True)
    code_language = models.CharField(max_length=200)
    question_name = models.CharField(max_length=200)
    section = models.ForeignKey(AssessmentSection,null=True)
    score = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=100)
    unique_id = models.CharField(max_length=100,null=True)
    submitted = models.BooleanField(default=False)

    def __unicode__(self):
       return self.user.username


040-43468888