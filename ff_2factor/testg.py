"""
Shows basic usage of the Gmail API.

Lists the user's Gmail labels.
"""
from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import base64
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from apiclient import errors, discovery
import mimetypes
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
import httplib2


# Setup the Gmail API
SCOPES = 'https://mail.google.com/'
store = file.Storage('token.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('gmail', 'v1', http=creds.authorize(Http()))

# Call the Gmail API
# results = service.users().labels().list(userId='me').execute()
# labels = results.get('labels', [])
# if not labels:
#     print('No labels found.')
# else:
#     print('Labels:')
#     for label in labels:
#         print(label['name'])

credentials = creds

subject = "test"
sender = "no-reply@cantileverlabs.com"
to = "abhijeet987@live.co.uk"
msgPlain = "Test"

msg = MIMEMultipart('alternative')
msg['Subject'] = subject
msg['From'] = sender
msg['To'] = to
msg.attach(MIMEText(msgPlain, 'plain'))
msg_built = {'raw': base64.urlsafe_b64encode(msg.as_string())}


http = credentials.authorize(httplib2.Http())
service = discovery.build('gmail', 'v1', http=http)
user_id = "me"

try:
	message = (service.users().messages().send(userId=user_id, body=msg_built).execute())
	print('Message Id: %s' % message['id'])
	
except errors.HttpError as error:
	print('An error occurred: %s' % error)
	
